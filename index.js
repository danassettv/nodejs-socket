const app = require("express")();
const http = require("http").Server(app);
const io = require("socket.io")(http);
const MongoClient = require("mongodb").MongoClient;

let database;

MongoClient.connect(
  "mongodb://admin:nimda@ds161021.mlab.com:61021/test-db",
  (err, db) => {
    if (!err) {
      database = db;
    }
  }
);

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/index.html");
});

io.on("connection", function(socket) {
  io.emit("record id", socket.id);
  socket.on("record analytics", function(data) {
    const collection = database.collection("analytics");

    collection.findOneAndUpdate(
      {
        sessionId: data.sessionId
      },
      data,
      { upsert: true },
      function(err, result) {
        io.emit("display analytics", JSON.stringify(data));
      }
    );
  });
  socket.on("disconnect", function(socket) {
    console.log("Disconnected");
  });
});

http.listen(process.env.PORT || 3000, () => {
  console.log("Listening on *:3000", process.env.PORT);
});
